import React from "react";
import { App } from "../src/App";
import { About } from "../src/routes/About/About";

export default function page() {
  return (
    <App>
      <About />
    </App>
  );
}
