import React from "react";
import App from "next/app";
import Router from "next/router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { configure } from "mobx";
import { Provider, useStaticRendering } from "mobx-react";
import { withMobx } from "next-mobx-wrapper";
import { appWithTranslation } from "../i18n";
import * as getStores from "../src/stores/rootStore";
import "normalize.css/normalize.css";
import "../src/assets/styles/settings.scss";
import "react-notifications/lib/notifications.css";

const isServer = !process.browser;

configure({ enforceActions: "observed" });
useStaticRendering(isServer);

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

class MyApp extends App {
  render() {
    const { Component, pageProps, store } = this.props;

    return (
      <Provider {...store}>
        <Component {...pageProps} />
      </Provider>
    );
  }
}

export default appWithTranslation(withMobx(getStores)(MyApp));
