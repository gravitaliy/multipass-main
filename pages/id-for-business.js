import React from "react";
import { App } from "../src/App";
import { IdForBusiness } from "../src/routes/IdForBusiness/IdForBusiness";

export default function page() {
  return (
    <App>
      <IdForBusiness />
    </App>
  );
}
