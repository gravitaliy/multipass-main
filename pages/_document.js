/* eslint-disable react/no-danger */
import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
          <ExternalScripts />
        </body>
      </Html>
    );
  }
}

export default MyDocument;

const ExternalScripts = () => {
  return (
    <>
      {/* vk */}
      <script
        dangerouslySetInnerHTML={{
          __html: `
          !(function() {
            var t = document.createElement("script");
            (t.type = "text/javascript"),
              (t.async = !0),
              (t.src = "https://vk.com/js/api/openapi.js?166"),
              (t.onload = function() {
                VK.Retargeting.Init("VK-RTRG-451950-2tp6t"), VK.Retargeting.Hit();
              }),
              document.head.appendChild(t);
          })();
        `,
        }}
      />
      <noscript>
        <img
          src="https://vk.com/rtrg?p=VK-RTRG-451950-2tp6t"
          style={{ position: "fixed", left: -999 }}
          alt=""
        />
      </noscript>

      {/* Facebook Pixel Code */}
      <script
        dangerouslySetInnerHTML={{
          __html: `
          !(function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
              n.callMethod
                ? n.callMethod.apply(n, arguments)
                : n.queue.push(arguments);
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = "2.0";
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s);
          })(
            window,
            document,
            "script",
            "https://connect.facebook.net/en_US/fbevents.js",
          );
          fbq("init", "2858605747517418");
          fbq("track", "PageView");
        `,
        }}
      />
      <noscript>
        <img
          height="1"
          width="1"
          style={{ display: "none" }}
          src="https://www.facebook.com/tr?id=2858605747517418&ev=PageView&noscript=1"
          alt=""
        />
      </noscript>

      {/* Rating Mail.ru counter */}
      <script
        dangerouslySetInnerHTML={{
          __html: `
          var _tmr = window._tmr || (window._tmr = []);
          _tmr.push({
            id: "3160414",
            type: "pageView",
            start: new Date().getTime(),
            pid: "USER_ID",
          });
          (function(d, w, id) {
            if (d.getElementById(id)) return;
            var ts = d.createElement("script");
            ts.type = "text/javascript";
            ts.async = true;
            ts.id = id;
            ts.src = "https://top-fwz1.mail.ru/js/code.js";
            var f = function() {
              var s = d.getElementsByTagName("script")[0];
              s.parentNode.insertBefore(ts, s);
            };
            if (w.opera == "[object Opera]") {
              d.addEventListener("DOMContentLoaded", f, false);
            } else {
              f();
            }
          })(document, window, "topmailru-code");
        `,
        }}
      />
      <noscript>
        <div>
          <img
            src="https://top-fwz1.mail.ru/counter?id=3160414;js=na"
            style={{ boder: 0, position: "absolute", left: -9999 }}
            alt="Top.Mail.Ru"
          />
        </div>
      </noscript>

      {/* Widget chat */}
      <script
        dangerouslySetInnerHTML={{
          __html: `
          function initFreshChat() { window.fcWidget.init({ token: "8e3ec343-f965-48c5-9c22-a65f4f9daec4", host: "https://wchat.freshchat.com" }); } function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
        `,
        }}
      />
    </>
  );
};
