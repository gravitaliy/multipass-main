import React from "react";
import { App } from "../src/App";
import { IdForPeople } from "../src/routes/IdForPeople/IdForPeople";

export default function page() {
  return (
    <App>
      <IdForPeople />
    </App>
  );
}
