import React from "react";
import { App } from "../../src/App";
import { NewsArticle } from "../../src/routes/NewsArticle/NewsArticle";

export default function page() {
  return (
    <App>
      <NewsArticle />
    </App>
  );
}
