import React from "react";
import { App } from "../../src/App";
import { News } from "../../src/routes/News/News";

export default function page() {
  return (
    <App>
      <News />
    </App>
  );
}
