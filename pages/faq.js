import React from "react";
import { App } from "../src/App";
import { Faq } from "../src/routes/Faq/Faq";

export default function page() {
  return (
    <App>
      <Faq />
    </App>
  );
}
