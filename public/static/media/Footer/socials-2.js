import React from "react";

export const AskSvg = ({ circleFill, opacity }) => {
  return (
    <svg
      width="78"
      height="80"
      viewBox="0 0 78 80"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_d)">
        <ellipse
          cx="39"
          cy="32"
          rx="25"
          ry="26"
          fill={circleFill || "#1DA1F2"}
          fillOpacity={opacity || "0.07"}
        />
      </g>
      <g clipPath="url(#clip0)">
        <path
          d="M34.8759 40.1292C43.1782 40.1292 47.7183 33.8762 47.7183 28.4542C47.7183 28.2766 47.7143 28.0997 47.7056 27.9238C48.5868 27.3446 49.3528 26.6217 49.9571 25.799C49.1484 26.1258 48.2779 26.3457 47.365 26.445C48.2969 25.9369 49.0124 25.1334 49.3498 24.1754C48.4777 24.6454 47.5119 24.9869 46.4836 25.1714C45.66 24.3738 44.4873 23.875 43.1888 23.875C40.6963 23.875 38.6749 25.7126 38.6749 27.9778C38.6749 28.2999 38.7145 28.613 38.7919 28.9135C35.0405 28.7418 31.714 27.1091 29.488 24.6262C29.1004 25.2326 28.8768 25.9371 28.8768 26.6887C28.8768 28.1122 29.6738 29.3691 30.8855 30.1043C30.1449 30.0835 29.4494 29.8987 28.8412 29.5911C28.8406 29.6083 28.8406 29.6251 28.8406 29.6434C28.8406 31.6308 30.3964 33.29 32.4619 33.666C32.0825 33.7598 31.6834 33.8103 31.2717 33.8103C30.9813 33.8103 30.6983 33.7844 30.4232 33.7363C30.9979 35.3667 32.6643 36.5531 34.6399 36.5864C33.095 37.6871 31.1488 38.3428 29.0338 38.3428C28.6699 38.3428 28.3103 38.3239 27.957 38.286C29.9546 39.45 32.3268 40.1293 34.876 40.1293"
          fill="#1DA1F2"
        />
      </g>
      <defs>
        <filter
          id="filter0_d"
          x="0"
          y="0"
          width="78"
          height="80"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="8" />
          <feGaussianBlur stdDeviation="7" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0.113725 0 0 0 0 0.631373 0 0 0 0 0.94902 0 0 0 0.07 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
        <clipPath id="clip0">
          <rect x="28" y="22" width="22" height="20" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};
