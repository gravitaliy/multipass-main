import React from "react";

export const FacebookSvg = ({ circleFill, opacity }) => {
  return (
    <svg
      width="78"
      height="78"
      viewBox="0 0 78 78"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_d)">
        <circle
          cx="39"
          cy="31"
          r="25"
          fill={circleFill || "#4267B2"}
          fillOpacity={opacity || "0.07"}
        />
      </g>
      <path
        d="M40.375 27.5625V24.8125C40.375 24.0535 40.991 23.4375 41.75 23.4375H43.125V20H40.375C38.0966 20 36.25 21.8466 36.25 24.125V27.5625H33.5V31H36.25V42H40.375V31H43.125L44.5 27.5625H40.375Z"
        fill="#4267B2"
      />
      <defs>
        <filter
          id="filter0_d"
          x="0"
          y="0"
          width="78"
          height="78"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="8" />
          <feGaussianBlur stdDeviation="7" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0.136719 0 0 0 0 0.457031 0 0 0 0 0.9375 0 0 0 0.07 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
      </defs>
    </svg>
  );
};
