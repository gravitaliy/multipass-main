import { useTranslation } from "react-i18next";
import { routesList } from "./routesList";

export const navData = () => {
  const { t } = useTranslation("nav");

  return [
    { label: t("nav-1"), link: routesList.ABOUT },
    { label: t("nav-2"), link: routesList.ID_FOR_PEOPLE },
    { label: t("nav-3"), link: routesList.ID_FOR_BUSINESS },
    { label: t("nav-4"), link: routesList.FAQ },
  ].map((item, i) => ({ ...item, id: i }));
};
