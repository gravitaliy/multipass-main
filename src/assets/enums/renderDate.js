import moment from "moment";

export const renderDate = (date, format = "DD.MM.YYYY") => {
  const getDate = moment(date);
  return getDate.format(format);
};
