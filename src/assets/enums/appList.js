export const appData = [
  {
    img: "/static/media/_general/App-Store-button.svg",
    href: "https://apps.apple.com/tt/app/multipass-beta/id1503997959",
    label: "App Store",
  },
  {
    img: "/static/media/_general/Google-Play-button.svg",
    href: "https://play.google.com/store/apps/details?id=com.multipass",
    label: "Google Play",
  },
].map((item, i) => ({ ...item, id: i }));
