import React from "react";
import { routesList } from "./routesList";

export const newsData = [
  {
    img: "/static/media/News/news-1.png",
    title: "New multipass update!",
    description:
      "Short demo text demo text demo text demo text demo text demo text demo text.",
    date: new Date("2020.04.05"),
    time: "3 min read",
    content: () => (
      <>
        <p>
          Stay up-to-date with the latest news and press releases from
          Multipass. Feel free to browse around and check out some of the
          feature articles, videos, and blog posts written by our team. Stay
          up-to-date with the latest news and press releases from Multipass.
          Feel free to browse around and check out some of the feature articles,
          videos, and blog posts written by our team. Stay up-to-date with the
          latest news and press releases from Multipass. Feel free to browse
          around and check out some of the feature articles, videos, and blog
          posts written by our team.
        </p>
        <p>
          Stay up-to-date with the latest news and press releases from
          Multipass. Feel free to browse around and check out some of the
          feature articles, videos, and blog posts written by our team. Stay
          up-to-date with the latest news and press releases from Multipass.
        </p>
      </>
    ),
  },
  {
    img: "/static/media/News/news-2.png",
    title: "New multipass update!",
    description:
      "Short demo text demo text demo text demo text demo text demo text demo text.",
    date: new Date("2020.04.05"),
    time: "3 min read",
    content: () => (
      <>
        <p>some text</p>
      </>
    ),
  },
  {
    img: "/static/media/News/news-3.png",
    title: "New multipass update!",
    description:
      "Short demo text demo text demo text demo text demo text demo text demo text.",
    date: new Date("2020.04.05"),
    time: "3 min read",
    content: () => (
      <>
        <p>some text</p>
      </>
    ),
  },
  {
    img: "/static/media/News/news-4.png",
    title: "New multipass update!",
    description:
      "Short demo text demo text demo text demo text demo text demo text demo text.",
    date: new Date("2020.04.05"),
    time: "3 min read",
    content: () => (
      <>
        <p>some text</p>
      </>
    ),
  },
  {
    img: "/static/media/News/news-5.png",
    title: "New multipass update!",
    description:
      "Short demo text demo text demo text demo text demo text demo text demo text.",
    date: new Date("2020.04.05"),
    time: "3 min read",
    content: () => (
      <>
        <p>some text</p>
      </>
    ),
  },
  {
    img: "/static/media/News/news-6.png",
    title: "New multipass update!",
    description:
      "Short demo text demo text demo text demo text demo text demo text demo text.",
    date: new Date("2020.04.05"),
    time: "3 min read",
    content: () => (
      <>
        <p>some text</p>
      </>
    ),
  },
].map((item, i) => ({
  ...item,
  id: i,
  href: routesList.NEWS,
}));
