export const routesList = {
  CREATE_PASSPORT: "https://create.multipass.org",
  CHECK_PASSPORT: "/CheckPassport",
  PARTIAL_PASSPORT: "/PartialPassport",
  QR_AUTH_DEMO: "/QRAuth",
  ABOUT: "/about",
  ID_FOR_PEOPLE: "/id-for-people",
  ID_FOR_BUSINESS: "/id-for-business",
  FAQ: "/faq",
  NEWS: "/news/",
};

const wdia = "https://wdia.org";

export const routesListWdia = {
  JOIN_US: `${wdia}/join-us`,
  OUR_NETWORK: `${wdia}/our-network`,
};
