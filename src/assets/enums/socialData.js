export const socialsData = [
  {
    link: "https://www.instagram.com/wdiaorg/",
    img: "/static/media/Footer/socials-1.svg",
    imgColor: "/static/media/Footer/socials-1-color.svg",
  },
  {
    link: "https://twitter.com/wdia_org",
    img: "/static/media/Footer/socials-2.svg",
    imgColor: "/static/media/Footer/socials-2-color.svg",
  },
  {
    link: "https://www.facebook.com/wdia.org",
    img: "/static/media/Footer/socials-3.svg",
    imgColor: "/static/media/Footer/socials-3-color.svg",
  },
].map((item, i) => ({ ...item, id: i }));
