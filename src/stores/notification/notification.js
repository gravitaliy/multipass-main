import { observable } from "mobx";
import { getOrCreateStore } from "next-mobx-wrapper";
import { notificationTypes } from "../../assets/enums/notification.type";

class NotificationStore {
  @observable notification = {
    title: "Success",
    message: "",
    type: null,
    duration: null,
  };

  setNotification({ title, message, type, duration }) {
    this.notification = {
      title,
      message,
      type: type || notificationTypes.success,
      duration: duration || 5000,
    };
  }
}

export const getNotificationStore = getOrCreateStore(
  "notificationStore",
  NotificationStore,
);
