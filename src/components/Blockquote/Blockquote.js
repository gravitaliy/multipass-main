import React, { memo } from "react";
import clsx from "clsx";
import styles from "./Blockquote.module.scss";

export const Blockquote = memo(({ children }) => {
  return (
    <div className={styles["blockquote-block"]}>
      <div className="c-container">
        <div className={styles["blockquote-wrap"]}>
          <blockquote className={clsx(styles.blockquote, "c-description-gen")}>
            {children}
          </blockquote>
        </div>
      </div>
    </div>
  );
});
