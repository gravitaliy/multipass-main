import React, { memo } from "react";
import clsx from "clsx";
import styles from "./Input.module.scss";

export const Input = memo(props => {
  const setItem = e => {
    props.setItem(e.target.name, e.target.value);
  };

  const inputParams = {
    className: styles["input-component-item"],
    onChange: props.onChange || setItem,
    value: props.value,
    name: props.name,
    required: !!props.required,
    minLength: props.minLength || 1,
    maxLength: 200,
    title: props.title || "",
    placeholder: `${props.placeholder}${props.required ? "*" : ""}`,
    disabled: props.disabled,
    onBlur: props.onBlur,
    style: props.style,
  };

  return (
    <div className={clsx(props.fillHeightStyle && styles["fill-height"])}>
      {props.heading && (
        <p className={styles["input-component-heading"]}>{props.heading}</p>
      )}
      <div
        className={clsx(
          styles["input-component-item-wrap"],
          props.fillHeightStyle && styles["fill-height"],
          "flex",
          "start",
        )}
      >
        {props.type === "textarea" ? (
          <textarea
            className={inputParams.className}
            onChange={inputParams.onChange}
            value={inputParams.value}
            name={inputParams.name}
            required={inputParams.required}
            minLength={inputParams.minLength}
            maxLength={inputParams.maxLength}
            title={inputParams.title}
            placeholder={inputParams.placeholder}
            disabled={inputParams.disabled}
            onBlur={inputParams.onBlur}
            style={inputParams.style}
          />
        ) : (
          <input
            className={inputParams.className}
            onChange={inputParams.onChange}
            value={inputParams.value}
            name={inputParams.name}
            required={inputParams.required}
            minLength={inputParams.minLength}
            maxLength={inputParams.maxLength}
            title={inputParams.title}
            placeholder={inputParams.placeholder}
            disabled={inputParams.disabled}
            onBlur={inputParams.onBlur}
            style={inputParams.style}
            type={props.type || "text"}
            pattern={props.pattern || ".*"}
          />
        )}
      </div>
    </div>
  );
});
