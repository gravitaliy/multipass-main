import React, { memo } from "react";
import styles from "./Advantages.module.scss";

export const Advantages = memo(({ data, children }) => {
  return (
    <div className={styles.advantages}>
      <div className="c-container">
        <div className="c-title-h2">{children}</div>
        <div className={styles.advantages__items}>
          {data.map(item => (
            <div
              className={styles["advantages__items-item"]}
              key={item.id}
              style={{
                width: `${100 / data.length - 1}%`,
              }}
            >
              <div className="c-description-gen">{item.text}</div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
});
