import React, { memo } from "react";
import Link from "next/link";
import styles from "./Card.module.scss";
import { renderDate } from "../../assets/enums/renderDate";

export const Card = memo(({ data }) => {
  return (
    <div className={styles["news-list-card"]}>
      {data.map(item => (
        <Link href={`${item.href}[id]`} as={item.href + item.id} key={item.id}>
          <a className={styles["news-card"]} href={item.href + item.id}>
            <div className={styles["news-card-img-wrap"]}>
              <img
                className={styles["news-card-img"]}
                src={item.img}
                alt="news-card"
              />
            </div>
            <div className={styles["news-card-container"]}>
              <div>
                <p className={styles["news-card-title"]}>{item.title}</p>
                <p className={styles["news-card-description"]}>
                  {item.description}
                </p>
              </div>
              <div className={styles["news-card-footer"]}>
                <p className={styles["news-card-date"]}>
                  {renderDate(item.date, "MMMM DD, YYYY")}
                </p>
                <p className={styles["news-card-time"]}>{item.time}</p>
              </div>
            </div>
          </a>
        </Link>
      ))}
    </div>
  );
});
