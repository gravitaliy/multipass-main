import React, { memo } from "react";
import clsx from "clsx";
import Link from "next/link";
import styles from "./ButtonMain.module.scss";

export const ButtonMain = memo(props => {
  const classNameOutline = `c-btn-${props.outline ? "outline-" : ""}`;
  const classNameColor = props.color;
  const classNameSize = props.size || "";
  const className = clsx(
    styles["c-btn"],
    styles[`${classNameOutline}${classNameColor}`],
    styles[classNameSize],
  );

  const InnerBtn = () => (
    <button className={className} type="button" onClick={props.onClick}>
      {props.label}
    </button>
  );

  if (!props.onSubmit) {
    if (props.internalLink) {
      return (
        <Link href={props.href || ""}>
          <a style={props.style} href={props.href || ""}>
            <InnerBtn />
          </a>
        </Link>
      );
    }
    if (!props.href) {
      return <InnerBtn />;
    }
    return (
      <a
        href={props.href || ""}
        style={props.style}
        target={props.target}
        rel={props.rel}
      >
        <InnerBtn />
      </a>
    );
  }
  return (
    <button className={className} type="submit" onSubmit={props.onSubmit}>
      {props.label}
    </button>
  );
});
