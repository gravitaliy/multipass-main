import React, { memo } from "react";
import clsx from "clsx";
import { socialsData } from "../../assets/enums/socialData";
import styles from "./SocialBlocks.module.scss";

export const SocialBlocks = memo(({ isImgColor, round, vertical }) => {
  return (
    <div className={clsx(styles["social-blocks"], vertical && styles.vertical)}>
      {socialsData.map(item => (
        <a
          className={clsx(
            styles["socials-link"],
            round && styles.round,
            vertical && styles.vertical,
          )}
          href={item.link}
          key={item.id}
          target="_blank"
          rel="noopener noreferrer"
        >
          <img
            className="fix-img"
            src={isImgColor ? item.imgColor : item.img}
            alt=""
          />
        </a>
      ))}
    </div>
  );
});
