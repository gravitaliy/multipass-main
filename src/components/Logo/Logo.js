import React from "react";
import Link from "next/link";

export const Logo = () => {
  return (
    <Link href="/">
      <a style={{ display: "inline-block" }} href="/">
        <img className="fix-img" src="/static/media/Logo/logo.svg" alt="logo" />
      </a>
    </Link>
  );
};
