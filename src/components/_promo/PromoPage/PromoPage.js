import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import { ButtonMain } from "../../ButtonMain/ButtonMain";
import { routesList } from "../../../assets/enums/routesList";
import styles from "./PromoPage.module.scss";

export const PromoPage = memo(
  ({ children, title, btnCreate, bgPattern, className }) => {
    const { t } = useTranslation("common");

    return (
      <div
        className={clsx(
          styles["promo-page"],
          styles[`bg-pattern-${bgPattern}`],
        )}
      >
        <div className="c-container">
          <div className={clsx(styles["promo-page-inner"], className)}>
            <div className={styles["promo-page-title"]}>{title}</div>
            <div
              className={clsx(
                styles["promo-page-description"],
                "c-description-gen",
              )}
            >
              {children}
            </div>
            {btnCreate && (
              <ButtonMain
                href={routesList.CREATE_PASSPORT}
                target="_blank"
                rel="noreferrer noopener"
                label={t("create-multipass-btn")}
                color="blue"
              />
            )}
          </div>
        </div>
      </div>
    );
  },
);
