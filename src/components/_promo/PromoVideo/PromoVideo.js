import React, { memo } from "react";
import clsx from "clsx";
import styles from "./PromoVideo.module.scss";

export const PromoVideo = memo(({ title, description, video }) => {
  return (
    <div className={styles["promo-video"]}>
      <div className="c-container">
        <div className={styles["promo-video__text-wrap"]}>
          <div className={styles["promo-video__text"]}>
            <div
              className={clsx(styles["promo-video__text__title"], "c-title")}
            >
              {title.basic}
              <span className="green">{title.highlight}</span>
            </div>
            <div
              className={clsx(
                styles["promo-video__text__description"],
                "c-description-gen"
              )}
            >
              {description}
            </div>
          </div>
        </div>
      </div>
      <div className={styles["promo-video__video-wrap"]}>
        {/* eslint-disable-next-line */}
        <video
          autoPlay
          loop
          width="100%"
          style={{ display: "block" }}
          src={video}
        />
      </div>
    </div>
  );
});
