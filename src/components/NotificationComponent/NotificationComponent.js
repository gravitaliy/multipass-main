import React, { useContext } from "react";
import { MobXProviderContext, observer } from "mobx-react";
import {
  NotificationManager,
  NotificationContainer,
} from "react-notifications";

export const NotificationComponent = observer(() => {
  const { notificationStore } = useContext(MobXProviderContext);
  const { notification } = notificationStore;
  const { title, message, type, duration } = notification;

  if (type) {
    NotificationManager[type](title, message, duration, () => {});
  }

  return <NotificationContainer />;
});
