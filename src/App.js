import React from "react";
import Head from "next/head";
import { Header } from "./containers/Header/Header";
import { Footer } from "./containers/Footer/Footer";
import { NotificationComponent } from "./components/NotificationComponent/NotificationComponent";

export const App = ({ children }) => {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="description"
          content="Multipass: one crypto-passport to access all the services"
        />
        <meta name="theme-color" content="#3663f5" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#3663f5" />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap"
          rel="stylesheet"
        />
        <link
          rel="shortcut icon"
          sizes="16x16 24x24 32x32 48x48 64x64"
          href="/static/media/_general/favicon/favicon.ico"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/static/media/_general/favicon/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/static/media/_general/favicon/favicon-16x16.png"
        />
        {/* Mobile (Android, iOS & others) */}
        <link
          rel="apple-touch-icon"
          href="/static/media/_general/favicon/apple-touch-icon.png"
        />
        {/* iOS Settings */}
        <meta content="yes" name="apple-mobile-web-app-capable" />
        <meta
          name="apple-mobile-web-app-status-bar-style"
          content="black-translucent"
        />
        <title>MultiPass: Your digital freedom - globally</title>
      </Head>
      <NotificationComponent />
      <Header />
      <main>{children}</main>
      <Footer />
    </>
  );
};
