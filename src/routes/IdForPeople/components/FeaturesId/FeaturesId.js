import React, { memo } from "react";
import styles from "./FeaturesId.module.scss";

export const FeaturesId = memo(({ t }) => {
  const data = [
    {
      title: t("features-title-1"),
      img: "/static/media/IdForPeople/FeaturesId/item-1.svg",
      waitingTimeOld: 20,
      waitingLabelOld: t("features-time-min"),
      waitingTimeNew: 1,
      waitingLabelNew: t("features-time-min"),
    },
    {
      title: t("features-title-2"),
      img: "/static/media/IdForPeople/FeaturesId/item-2.svg",
      waitingTimeOld: 3,
      waitingLabelOld: t("features-time-days"),
      waitingTimeNew: 5,
      waitingLabelNew: t("features-time-min"),
    },
    {
      title: t("features-title-3"),
      img: "/static/media/IdForPeople/FeaturesId/item-3.svg",
      waitingTimeOld: 30,
      waitingLabelOld: t("features-time-min"),
      waitingTimeNew: 1,
      waitingLabelNew: t("features-time-min"),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <div className={styles["features-id"]}>
      <div className="c-container">
        <div className={styles["features-id__items"]}>
          {data.map(item => (
            <div className={styles["features-id__items-item"]} key={item.id}>
              <div className={styles["features-id__items-item__count"]}>
                {item.id + 1}
              </div>
              <div className={styles["features-id__items-item__title"]}>
                {item.title}
              </div>
              <div className={styles["features-id__items-item__img-wrap"]}>
                <img className="fix-img" src={item.img} alt="" />
              </div>
              <div className={styles["features-id__items-item__waiting-old"]}>
                <div
                  className={
                    styles["features-id__items-item__waiting-old__label"]
                  }
                >
                  {t("features-waiting-time")}
                </div>
                <div
                  className={
                    styles["features-id__items-item__waiting-old__line"]
                  }
                />
                <div
                  className={
                    styles["features-id__items-item__waiting-old__time"]
                  }
                >
                  {item.waitingTimeOld} {item.waitingLabelOld}
                </div>
              </div>
              <div className={styles["features-id__items-item__waiting-new"]}>
                <div
                  className={
                    styles["features-id__items-item__waiting-new__label"]
                  }
                >
                  {t("features-with-multipass")}
                </div>
                <div
                  className={
                    styles["features-id__items-item__waiting-new__line"]
                  }
                  style={{
                    flexGrow:
                      item.waitingLabelOld === item.waitingLabelNew
                        ? item.waitingTimeNew / item.waitingTimeOld
                        : 0,
                  }}
                />
                <div
                  className={
                    styles["features-id__items-item__waiting-new__time"]
                  }
                >
                  {item.waitingTimeNew} {item.waitingLabelNew}
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
});
