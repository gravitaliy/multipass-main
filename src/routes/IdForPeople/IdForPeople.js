import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import { PromoVideo } from "../../components/_promo/PromoVideo/PromoVideo";
import { Blockquote } from "../../components/Blockquote/Blockquote";
import { FeaturesId } from "./components/FeaturesId/FeaturesId";
import { Advantages } from "../../components/Advantages/Advantages";

export const IdForPeople = memo(() => {
  const { t } = useTranslation("id-for-people");

  const advantagesData = [
    { text: t("advantages-text-1") },
    { text: t("advantages-text-2") },
    { text: t("advantages-text-3") },
    { text: t("advantages-text-4") },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <>
      <PromoVideo
        title={{
          basic: t("title-1"),
          highlight: t("title-2"),
        }}
        description={t("description")}
        video="/static/media/_general/videos/IdForPeople.mp4"
      />
      <Blockquote>
        {t("blockquote-1")}
        <span className="blue"> {t("blockquote-2")} </span>
        {t("blockquote-3")}
        <span className="blue"> {t("blockquote-4")} </span>
        {t("blockquote-5")}
      </Blockquote>
      <FeaturesId t={t} />
      <Advantages data={advantagesData}>
        {t("advantages-1")} <span className="green"> {t("advantages-2")}</span>{" "}
        {t("advantages-3")}
      </Advantages>
    </>
  );
});
