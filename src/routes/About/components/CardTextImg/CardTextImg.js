import React, { memo } from "react";
import clsx from "clsx";
import styles from "./CardTextImg.module.scss";

export const CardTextImg = memo(({ data }) => {
  return (
    <div className={styles["card-text-img"]}>
      {data.map(item => (
        <div className={styles["card-text-img-item"]} key={item.id}>
          <div className={styles["card-text-img-item__img-wrap"]}>
            <img
              className={clsx(styles["card-image"], "fix-img")}
              src={item.img}
              alt=""
            />
          </div>
          <div className={styles["card-text-img-item__text"]}>
            <div className={styles["card-text-img-item__text__title"]}>
              {item.title}
            </div>
            <div className="c-description-gen">{item.description}</div>
          </div>
        </div>
      ))}
    </div>
  );
});
