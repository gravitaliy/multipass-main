import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import { PromoPage } from "../../components/_promo/PromoPage/PromoPage";
import { CardTextImg } from "./components/CardTextImg/CardTextImg";
import styles from "./About.module.scss";

export const About = memo(() => {
  const { t } = useTranslation("about");

  const cardTextImgData = [
    {
      img: "/static/media/About/card-1.svg",
      title: t("card-item-1-title"),
      description: t("card-item-1-description"),
    },
    {
      img: "/static/media/About/card-2.svg",
      title: t("card-item-2-title"),
      description: t("card-item-2-description"),
    },
    {
      img: "/static/media/About/card-3.svg",
      title: t("card-item-3-title"),
      description: t("card-item-3-description"),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <div className={styles["page-new"]}>
      <PromoPage title={t("title")} btnCreate bgPattern={1}>
        {t("description")}
      </PromoPage>
      <div className={styles["how-works"]}>
        <div className="c-container">
          <div className="c-title-h2">
            {t("cards-title-1")}{" "}
            <span className="blue">{t("cards-title-2")}</span>
          </div>
          <CardTextImg data={cardTextImgData} />
        </div>
      </div>
    </div>
  );
});
