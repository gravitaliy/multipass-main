import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import { Promo } from "./components/Promo/Promo";
import { About } from "./components/About/About";
import { Features } from "./components/Features/Features";
import { HowUse } from "./components/HowUse/HowUse";
import { Demo } from "./components/Demo/Demo";
import { Cases } from "./components/Cases/Cases";
import { Partnership } from "./components/Partnership/Partnership";

export const Main = memo(() => {
  const { t } = useTranslation(["index", "common", "form"]);

  return (
    <>
      <Promo t={t} />
      <About t={t} />
      <Features t={t} />
      <HowUse t={t} />
      <Demo t={t} />
      <Cases t={t} />
      <Partnership t={t} />
    </>
  );
});
