import React, { memo } from "react";
import clsx from "clsx";
import styles from "./About.module.scss";

export const About = memo(({ t }) => {
  return (
    <div className={styles.about}>
      <div className="c-container">
        <div className={clsx(styles["about-inner"], "flex", "justify-between")}>
          <div className={styles["about-left"]}>
            <div className={clsx(styles["about-title"], "c-title")}>
              {t("about-title")}
            </div>
            <div className={styles["about-description"]}>
              <p>{t("about-description-1")}</p>
              <p>{t("about-description-2")}</p>
            </div>
          </div>
          <div className={styles["about-right"]}>
            <img
              className={clsx(styles["about-right-img"], "fix-img")}
              src="/static/media/Main/About/about-bg.png"
              alt="app"
            />
          </div>
        </div>
      </div>
    </div>
  );
});
