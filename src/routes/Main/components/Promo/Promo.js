import React, { memo } from "react";
import { ButtonMain } from "../../../../components/ButtonMain/ButtonMain";
import { routesList } from "../../../../assets/enums/routesList";
import { appData } from "../../../../assets/enums/appList";
import styles from "./Promo.module.scss";

export const Promo = memo(({ t }) => {
  return (
    <div className={styles.promo}>
      <div className="c-container">
        <div className={styles["promo-inner"]}>
          <span className={styles["cloud-left"]} />
          <span className={styles["cloud-right"]} />
          <div className={styles["promo-title"]}>{t("promo-title")}</div>
          <div className={styles["promo-description"]}>
            <div className="xs-span">{t("promo-description-1")}</div>
            <div className="xs-span">{t("promo-description-2")}</div>
            <div className="xs-span">{t("promo-description-3")}</div>
          </div>
          <ButtonMain
            href={routesList.CREATE_PASSPORT}
            target="_blank"
            rel="noreferrer noopener"
            label={t("common:create-multipass-btn")}
            color="blue"
          />
          <div className={styles["promo-app-wrap"]}>
            {appData.map(item => (
              <a
                className={styles["promo-app-item"]}
                href={item.href}
                target="_blank"
                rel="noreferrer noopener"
                key={item.id}
              >
                <img className="fix-img" src={item.img} alt="button" />
              </a>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
});
