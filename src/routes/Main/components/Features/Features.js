import React, { memo } from "react";
import clsx from "clsx";
import styles from "./Features.module.scss";

export const Features = memo(({ t }) => {
  const featuresItemData = [
    {
      icon: "/static/media/Main/Features/icon-1.svg",
      label: t("features-1"),
    },
    {
      icon: "/static/media/Main/Features/icon-2.svg",
      label: t("features-2"),
    },
    {
      icon: "/static/media/Main/Features/icon-3.svg",
      label: t("features-3"),
    },
    {
      icon: "/static/media/Main/Features/icon-4.svg",
      label: t("features-4"),
    },
    {
      icon: "/static/media/Main/Features/icon-5.svg",
      label: t("features-5"),
    },
    {
      icon: "/static/media/Main/Features/icon-6.svg",
      label: t("features-6"),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <div className={styles.features}>
      <div className="c-container">
        <div className={clsx(styles["features-title"], "c-title")}>
          {t("features-title")}
        </div>
        <div className={styles["features-items"]}>
          {featuresItemData.map(item => (
            <div
              className={clsx(
                styles["features-items-item"],
                "flex",
                "align-center",
              )}
              key={item.id}
            >
              <div className={styles["features-item-icon"]}>
                <img className="fix-img" src={item.icon} alt="" />
              </div>
              <div className={styles["features-item-text"]}>{item.label}</div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
});
