import React, { memo } from "react";
import clsx from "clsx";
import styles from "./Cases.module.scss";

export const Cases = memo(({ t }) => {
  const casesItemData = [
    {
      color: "red",
      img: "/static/media/Main/Cases/img-1.svg",
      title: t("cases-item-1-title"),
      description: t("cases-item-1-description"),
    },
    {
      color: "blue",
      img: "/static/media/Main/Cases/img-2.svg",
      title: t("cases-item-2-title"),
      description: t("cases-item-2-description"),
    },
    {
      color: "yellow",
      img: "/static/media/Main/Cases/img-3.svg",
      title: t("cases-item-3-title"),
      description: t("cases-item-3-description"),
    },
    {
      color: "grey",
      img: "/static/media/Main/Cases/img-4.svg",
      title: t("cases-item-4-title"),
      description: t("cases-item-4-description"),
    },
    {
      color: "orange",
      img: "/static/media/Main/Cases/img-5.svg",
      title: t("cases-item-5-title"),
      description: t("cases-item-5-description"),
    },
    {
      color: "green",
      img: "/static/media/Main/Cases/img-6.svg",
      title: t("cases-item-6-title"),
      description: t("cases-item-6-description"),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <div className={styles.cases}>
      <div className="c-container">
        <div className="c-title">{t("cases-title")}</div>
        <div className={styles["cases-items"]}>
          {casesItemData.map(item => (
            <div className={styles["cases-items-item"]} key={item.id}>
              <div className={styles["cases-item-img"]}>
                <img className="fix-img" src={item.img} alt="" />
              </div>
              <div
                className={clsx(styles["cases-item-title"], styles[item.color])}
              >
                {item.title}
              </div>
              <div className={styles["cases-item-description"]}>
                {item.description}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
});
