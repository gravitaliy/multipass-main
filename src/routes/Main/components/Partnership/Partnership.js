import React, { memo, useState } from "react";
import { ButtonMain } from "../../../../components/ButtonMain/ButtonMain";
import { PartnershipForm } from "./components/PartnershipForm/PartnershipForm";
import styles from "./Partnership.module.scss";

export const Partnership = memo(({ t }) => {
  const [isShowForm, setShowForm] = useState(false);

  const showForm = () => {
    setShowForm(!isShowForm);
  };

  return (
    <div className={styles.partnership}>
      <div className="c-container">
        <div className="c-title">{t("partnership-title")}</div>
        <div className="c-description">{t("partnership-description")}</div>
        {isShowForm ? (
          <PartnershipForm isShowForm closeForm={showForm} t={t} />
        ) : (
          <div className="text-center">
            <ButtonMain
              label={t("partnership-btn")}
              color="blue"
              onClick={showForm}
            />
          </div>
        )}
      </div>
    </div>
  );
});
