import React, { useState, useContext, memo } from "react";
import Link from "next/link";
import { runInAction } from "mobx";
import { MobXProviderContext } from "mobx-react";
import axios from "axios";
import clsx from "clsx";
import { ButtonMain } from "../../../../../../components/ButtonMain/ButtonMain";
import { InputMain } from "../../../InputMain/InputMain";
import { notificationTypes } from "../../../../../../assets/enums/notification.type";
import styles from "./PartnershipForm.module.scss";
import { routesList } from "../../../../../../assets/enums/routesList";

export const PartnershipForm = memo(({ closeForm, t }) => {
  const [state, setState] = useState({ source: 1 });

  const { notificationStore } = useContext(MobXProviderContext);

  const handleChange = e => {
    const { name, value } = e.target;
    setState(prev => ({ ...prev, [name]: value }));
  };

  const submitForm = e => {
    e.preventDefault();
    axios
      .post("/v1/question/add", state)
      .then(() => {
        runInAction(() => {
          notificationStore.setNotification({
            title: "Success!",
            message: t("form:response-success"),
          });
        });
        closeForm();
      })
      .catch(err => {
        runInAction(() => {
          notificationStore.setNotification({
            title: "Error!",
            message: err.response?.data?.data?.message,
            type: notificationTypes.error,
          });
        });
      });
  };

  return (
    <form className={styles["partnership-form"]} onSubmit={submitForm}>
      <div className={clsx(styles["partnership-form-row"], styles["col-2"])}>
        <InputMain
          placeholder={t("form:input-name")}
          name="firstname"
          onChange={handleChange}
          required
        />
        <InputMain
          placeholder={t("form:input-last-name")}
          name="lastname"
          onChange={handleChange}
          required
        />
      </div>

      <div className={clsx(styles["partnership-form-row"], styles["col-2"])}>
        <InputMain
          type="tel"
          name="phone"
          placeholder={t("form:input-phone")}
          onChange={handleChange}
          required
        />
        <InputMain
          name="companyName"
          placeholder={t("form:input-company-name")}
          onChange={handleChange}
        />
      </div>

      <div className={clsx(styles["partnership-form-row"], styles["col-2"])}>
        <InputMain
          type="email"
          name="email"
          placeholder={t("form:input-email")}
          onChange={handleChange}
          required
        />
        <InputMain
          name="message"
          placeholder={t("form:input-message")}
          onChange={handleChange}
          required
        />
      </div>

      <div className={styles["partnership-details"]}>
        {/* <span>{t("partnership-details")}</span> */}
        <Link href={routesList.ABOUT}>
          <a
            className={styles["partnership-details-link"]}
            href={routesList.ABOUT}
          >
            {t("partnership-details-link")}
          </a>
        </Link>
      </div>

      <div className="text-center">
        <ButtonMain
          label={t("partnership-btn")}
          color="blue"
          onSubmit={submitForm}
        />
      </div>
    </form>
  );
});
