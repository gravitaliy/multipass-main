import React from "react";
import styles from "./InputMain.module.scss";

export const InputMain = props => {
  return (
    <div className={styles["input-field"]}>
      <input
        className={styles["form-input"]}
        type={props.type || "text"}
        name={props.name}
        placeholder={`${props.placeholder}${props.required ? "*" : ""}`}
        required={!!props.required}
        onChange={props.onChange}
      />
    </div>
  );
};
