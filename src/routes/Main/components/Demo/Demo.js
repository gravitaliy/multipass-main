import React, { memo } from "react";
import { ButtonMain } from "../../../../components/ButtonMain/ButtonMain";
import { routesList } from "../../../../assets/enums/routesList";
import styles from "./Demo.module.scss";

export const Demo = memo(({ t }) => {
  return (
    <div className={styles.demo}>
      <div className="c-container">
        <div className="c-title">{t("demo-title")}</div>
        <div className="c-description">{t("demo-description")}</div>
        <div className="text-center">
          <ButtonMain
            internalLink
            href={routesList.ABOUT}
            label={t("demo-btn")}
            color="blue"
          />
        </div>
      </div>
    </div>
  );
});
