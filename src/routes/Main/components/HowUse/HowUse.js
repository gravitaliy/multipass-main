import React, { memo } from "react";
import clsx from "clsx";
import styles from "./HowUse.module.scss";

export const HowUse = memo(({ t }) => {
  return (
    <div className={styles["how-use"]}>
      <div className="c-container">
        <div className={clsx(styles["how-use-title"], "c-title")}>
          {t("how-use-title")}
        </div>
        <div className={styles["how-use-subtitle"]}>
          {t("how-use-subtitle")}
        </div>
        <div className={clsx(styles["how-use-description"], "c-description")}>
          {t("how-use-description-1")}
        </div>
        <div className={clsx(styles["how-use-description"], "c-description")}>
          {t("how-use-description-2")}
        </div>
        <div className={clsx(styles["how-use-description"], "c-description")}>
          {t("how-use-description-3")}
        </div>
        <div className={styles["how-use-tel-wrap"]}>
          <div className={styles["how-use-tel-text-xs"]}>
            <div
              className={clsx(
                styles["how-use-tel-text-xs-row"],
                styles["how-use-tel-text-xs-row-1"],
              )}
            >
              <div className={styles["how-use-tel-text-xs-item"]}>
                {t("how-use-item-1")}
              </div>
              <div className={styles["how-use-tel-text-xs-item"]}>
                {t("how-use-item-2")}
              </div>
              <div className={styles["how-use-tel-text-xs-item"]}>
                {t("how-use-item-3")}
              </div>
            </div>
            <div
              className={clsx(
                styles["how-use-tel-text-xs-row"],
                styles["how-use-tel-text-xs-row-2"],
              )}
            >
              <div className={styles["how-use-tel-text-xs-item"]}>
                {t("how-use-item-4")}
              </div>
              <div
                className={clsx(
                  styles["how-use-tel-text-xs-item"],
                  styles["large-item"],
                )}
              >
                {t("how-use-item-5")}
              </div>
              <div className={styles["how-use-tel-text-xs-item"]}>
                {t("how-use-item-6")}
              </div>
            </div>
          </div>

          <img
            className={clsx(styles["how-use-tel-img"], "fix-img")}
            src="/static/media/Main/HowUse/iphone.png"
            alt="iphone"
          />

          <div className={styles["how-use-tel-text"]}>
            <div
              className={clsx(
                styles["how-use-tel-text-row"],
                styles["how-use-tel-text-row-1"],
              )}
            >
              <div className={styles["how-use-tel-text-item"]}>
                {t("how-use-item-1")}
              </div>
              <div className={styles["how-use-tel-text-item"]}>
                {t("how-use-item-4")}
              </div>
            </div>
            <div
              className={clsx(
                styles["how-use-tel-text-row"],
                styles["how-use-tel-text-row-2"],
              )}
            >
              <div className={styles["how-use-tel-text-item"]}>
                {t("how-use-item-6")}
              </div>
              <div className={styles["how-use-tel-text-item"]}>
                {t("how-use-item-3")}
              </div>
            </div>
            <div
              className={clsx(
                styles["how-use-tel-text-row"],
                styles["how-use-tel-text-row-3"],
              )}
            >
              <div className={styles["how-use-tel-text-item"]}>
                {" "}
                {t("how-use-item-2")}
              </div>
              <div className={styles["how-use-tel-text-item"]}>
                {t("how-use-item-5")}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});
