import React, { memo } from "react";
import { useRouter } from "next/router";
import { Card } from "../../components/Card/Card";
import { newsData } from "../../assets/enums/newsData";
import { SocialBlocks } from "../../components/SocialBlocks/SocialBlocks";
import { renderDate } from "../../assets/enums/renderDate";
import styles from "./NewsArticle.module.scss";

export const NewsArticle = memo(() => {
  const router = useRouter();
  const { id } = router.query;
  const activeNews = newsData[id];

  return (
    <div className={styles["promo-update"]}>
      <div className="c-container">
        <div className={styles["promo-container-update"]}>
          <img
            className={styles["promo-image-update"]}
            src={activeNews.img}
            alt="update"
          />
          <h1 className={styles["promo-update-title"]}>{activeNews.title}</h1>
          <ul className={styles["promo-update-date-info"]}>
            <li className={styles["not-list-style-type"]}>
              <p className={styles["promo-update-date"]}>
                {renderDate(activeNews.date, "MMMM DD, YYYY")}
              </p>
            </li>
            <li className={styles["promo-update-item"]}>
              <p className={styles["promo-update-date"]}>{activeNews.time}</p>
            </li>
          </ul>
          <div className={styles["promo-update-content"]}>
            <div className={styles["promo-update-content-inner"]}>
              {activeNews.content()}
            </div>
            <SocialBlocks isImgColor round vertical />
          </div>
        </div>
        <div style={{ marginTop: 70 }}>
          <Card data={newsData} />
        </div>
      </div>
    </div>
  );
});
