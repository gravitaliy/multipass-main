import React, { memo } from "react";
import { Card } from "../../components/Card/Card";
import styles from "./News.module.scss";
import { PromoPage } from "../../components/_promo/PromoPage/PromoPage";
import { newsData } from "../../assets/enums/newsData";

export const News = memo(() => {
  return (
    <>
      <PromoPage
        title="News"
        bgPattern={2}
        className={styles["news-promo-inner"]}
      >
        Stay up-to-date with the latest news and press releases from Multipass.
        Feel free to browse around and check out some of the feature articles,
        videos, and blog posts written by our team.
      </PromoPage>
      <section className={styles["news-items-section"]}>
        <div className="c-container">
          <Card data={newsData} />
        </div>
      </section>
    </>
  );
});
