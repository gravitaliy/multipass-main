import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import { PromoVideo } from "../../components/_promo/PromoVideo/PromoVideo";
import { Blockquote } from "../../components/Blockquote/Blockquote";
import { Advantages } from "../../components/Advantages/Advantages";
import styles from "./IdForBusiness.module.scss";

export const IdForBusiness = memo(() => {
  const { t } = useTranslation("id-for-business");

  const advantagesData = [
    { text: t("advantages-text-1") },
    { text: t("advantages-text-2") },
    { text: t("advantages-text-3") },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <>
      <PromoVideo
        title={{
          basic: t("title-1"),
          highlight: t("title-2"),
        }}
        description={t("description")}
        video="static/media/_general/videos/IdForBusiness.mp4"
      />
      <Blockquote>
        {t("blockquote-1")}
        <span className="blue"> {t("blockquote-2")} </span>
        {t("blockquote-3")}
      </Blockquote>
      <div className={styles["description-block"]}>
        <div className="c-container">
          <div className={styles["description-block-row"]}>
            <div
              className={clsx(
                styles["description-block-row__text"],
                "c-description-gen",
              )}
            >
              {t("description-block")}
            </div>
            <div className={styles["description-block-row__img-wrap"]}>
              <img
                className="fix-img"
                src="/static/media/IdForBusiness/description.svg"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
      <Advantages data={advantagesData}>
        {t("advantages-1")} <span className="blue">{t("advantages-2")}</span>{" "}
        {t("advantages-3")}
      </Advantages>
    </>
  );
});
