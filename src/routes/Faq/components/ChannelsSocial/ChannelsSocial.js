import React, { memo } from "react";
import { SocialBlocks } from "../../../../components/SocialBlocks/SocialBlocks";
import styles from "./ChannelsSocial.module.scss";

export const ChannelsSocial = memo(({ t }) => {
  return (
    <div className={styles["channels-social-block"]}>
      <h4 className={styles["social-title"]}>{t("social")}</h4>
      <SocialBlocks isImgColor round />
    </div>
  );
});
