import React, { memo, useContext, useState } from "react";
import { runInAction } from "mobx";
import { MobXProviderContext } from "mobx-react";
import axios from "axios";
import { Input } from "../../../../components/Input/Input";
import { notificationTypes } from "../../../../assets/enums/notification.type";
import styles from "./Form.module.scss";

export const Form = memo(({ t }) => {
  const [state, setState] = useState({ source: 1 });

  const { notificationStore } = useContext(MobXProviderContext);

  const handleChange = e => {
    const { name, value } = e.target;
    setState(prev => ({ ...prev, [name]: value }));
  };

  const submitForm = e => {
    e.preventDefault();
    axios
      .post("/v1/question/add", state)
      .then(() => {
        runInAction(() => {
          notificationStore.setNotification({
            title: "Success!",
            message: t("form:response-success"),
          });
        });
      })
      .catch(err => {
        runInAction(() => {
          notificationStore.setNotification({
            title: "Error!",
            message: err.response?.data?.data?.message,
            type: notificationTypes.error,
          });
        });
      });
  };

  return (
    <div className={styles["faq-form-container"]}>
      <h4 className={styles["faq-form-title"]}>
        {t("form-title-1")} <span className="blue">{t("form-title-2")}</span>
      </h4>
      <p className={styles["faq-form-subtitle"]}>{t("form-subtitle")}</p>
      <div className={styles["faq-form"]}>
        <form onSubmit={submitForm}>
          <div className={styles["faq-form-container-content"]}>
            <div className={styles["faq-form-content"]}>
              <div className={styles["faq-form-input"]}>
                <Input
                  onChange={handleChange}
                  name="firstname"
                  placeholder={t("form:input-name")}
                  required
                />
              </div>
              <div className={styles["faq-form-input"]}>
                <Input
                  onChange={handleChange}
                  name="lastname"
                  placeholder={t("form:input-last-name")}
                  required
                />
              </div>
              <div className={styles["faq-form-input"]}>
                <Input
                  onChange={handleChange}
                  name="phone"
                  placeholder={t("form:input-phone")}
                  required
                />
              </div>
              <div className={styles["faq-form-input"]}>
                <Input
                  onChange={handleChange}
                  type="email"
                  name="email"
                  placeholder={t("form:input-email")}
                  required
                />
              </div>
              <div className={styles["faq-form-input"]}>
                <Input
                  onChange={handleChange}
                  name="subject"
                  placeholder={t("form:input-subject")}
                />
              </div>
            </div>
            <div className={styles["faq-form-content"]}>
              <Input
                type="textarea"
                name="message"
                onChange={handleChange}
                placeholder={t("form:input-message")}
                required
                fillHeightStyle
              />
            </div>
          </div>
          <div>
            <button className={styles["faq-send-button"]} type="submit">
              {t("form:input-submit")}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
});
