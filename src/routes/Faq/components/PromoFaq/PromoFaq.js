import React, { memo } from "react";
import clsx from "clsx";
import { ButtonMain } from "../../../../components/ButtonMain/ButtonMain";
import { routesList } from "../../../../assets/enums/routesList";
import styles from "./PromoFaq.module.scss";

export const PromoFaq = memo(({ t }) => {
  return (
    <div className={styles["promo-faq"]}>
      <div className="c-container">
        <div className={styles["promo-faq-inner"]}>
          <div className={styles["promo-faq-title"]}>{t("title")}</div>
          <div
            className={clsx(
              styles["promo-faq-description"],
              "c-description-gen",
            )}
          >
            {t("description")}
          </div>
          <div>
            <ButtonMain
              href={routesList.CREATE_PASSPORT}
              label={t("common:create-multipass-btn")}
              target="_blank"
              rel="noopener noreferrer"
              color="blue"
            />
          </div>
        </div>
      </div>
    </div>
  );
});
