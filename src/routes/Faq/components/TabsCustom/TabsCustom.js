import React, { memo } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import Collapsible from "react-collapsible";
import clsx from "clsx";
import { appData } from "../../../../assets/enums/appList";
import {
  routesList,
  routesListWdia,
} from "../../../../assets/enums/routesList";
import styles from "./TabsCustom.module.scss";

const CustomCollapsible = ({ children, trigger }) => {
  return (
    <Collapsible
      trigger={trigger}
      className={styles.collapsible}
      openedClassName={styles.collapsible}
      triggerClassName={styles.collapsible__trigger}
      triggerOpenedClassName={clsx(
        styles.collapsible__trigger,
        styles["is-open"],
      )}
      contentInnerClassName={styles["collapsible__content-inner"]}
      triggerTagName="div"
    >
      {children}
    </Collapsible>
  );
};

export const TabsCustom = memo(({ t }) => {
  return (
    <section className={styles["tabs-custom-section"]}>
      <div className="c-container">
        <Tabs
          className={styles.tabs}
          selectedTabClassName={styles["tabs__tab--selected"]}
        >
          <TabList className={styles["tabs__tab-list"]}>
            <Tab className={styles.tabs__tab}>{t("tab-title-1")}</Tab>
            <Tab className={styles.tabs__tab}>{t("tab-title-2")}</Tab>
            <Tab className={styles.tabs__tab}>{t("tab-title-3")}</Tab>
            <Tab className={styles.tabs__tab}>{t("tab-title-4")}</Tab>
          </TabList>
          {/* ============== START TAB 1 =============== */}
          <TabPanel>
            <CustomCollapsible trigger={t("tab-1-question-1")}>
              {t("tab-1-answer-1")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-1-question-2")}>
              {t("tab-1-answer-2")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-1-question-3")}>
              {t("tab-1-answer-3")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-1-question-4")}>
              {t("tab-1-answer-4")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-1-question-5")}>
              {t("tab-1-answer-5")}
              {appData.map(item => (
                <a
                  className={styles["faq-app"]}
                  href={item.href}
                  target="_blank"
                  rel="noreferrer noopener"
                  key={item.id}
                >
                  {item.label}
                </a>
              ))}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-1-question-6")}>
              {t("tab-1-answer-6-1")}
              <a
                className={styles["link-space"]}
                href={routesListWdia.OUR_NETWORK}
                target="_blank"
                rel="noopener noreferrer"
              >
                {routesListWdia.OUR_NETWORK}
              </a>
              {t("tab-1-answer-6-2")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-1-question-7")}>
              {t("tab-1-answer-7")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-1-question-8")}>
              {t("tab-1-answer-8")}
            </CustomCollapsible>
          </TabPanel>
          {/* ============== END TAB 1 ================= */}
          {/* ============== START TAB 2 =============== */}
          <TabPanel>
            <CustomCollapsible trigger={t("tab-2-question-1")}>
              {t("tab-2-answer-1-1")}
              <a
                className={styles["link-space"]}
                href={routesList.CREATE_PASSPORT}
                target="_blank"
                rel="noreferrer noopener"
              >
                {routesList.CREATE_PASSPORT}
              </a>
              {t("tab-2-answer-1-2")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-2-question-2")}>
              {t("tab-2-answer-2")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-2-question-3")}>
              <p>{t("tab-2-answer-3-1")}</p>
              <p>
                {t("tab-2-answer-3-2")}
                <a
                  className={styles["link-space"]}
                  href={routesListWdia.OUR_NETWORK}
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  {routesListWdia.OUR_NETWORK}
                </a>
              </p>
              <div>
                <b>{t("tab-2-answer-3-3")}</b>
              </div>
              {t("tab-2-answer-3-4")}
              <ol>
                <li>{t("tab-2-answer-3-5")}</li>
                <li>{t("tab-2-answer-3-6")}</li>
                <li>{t("tab-2-answer-3-7")}</li>
              </ol>
              <div>
                <b>{t("tab-2-answer-3-8")}</b>
              </div>
              {t("tab-2-answer-3-9")}
              <ol>
                <li>{t("tab-2-answer-3-10")}</li>
                <li>{t("tab-2-answer-3-11")}</li>
                <li>{t("tab-2-answer-3-12")}</li>
              </ol>
            </CustomCollapsible>
          </TabPanel>
          {/* ============== END TAB 2 ================= */}
          {/* ============== START TAB 3 =============== */}
          <TabPanel>
            <CustomCollapsible trigger={t("tab-3-question-1")}>
              {t("tab-3-answer-1")}
              <a
                className={styles["link-space"]}
                href={routesListWdia.OUR_NETWORK}
                target="_blank"
                rel="noreferrer noopener"
              >
                {routesListWdia.OUR_NETWORK}
              </a>
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-3-question-2")}>
              {t("tab-3-answer-2")}
              <a
                className={styles["link-space"]}
                href={routesListWdia.OUR_NETWORK}
                target="_blank"
                rel="noreferrer noopener"
              >
                {routesListWdia.OUR_NETWORK}
              </a>
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-3-question-3")}>
              {t("tab-3-answer-3")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-3-question-4")}>
              {t("tab-3-answer-4")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-3-question-5")}>
              {t("tab-3-answer-5")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-3-question-6")}>
              {t("tab-3-answer-6")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-3-question-7")}>
              {t("tab-3-answer-7")}
            </CustomCollapsible>
          </TabPanel>
          {/* ============== END TAB 3 ================= */}
          {/* ============== START TAB 4 =============== */}
          <TabPanel>
            <CustomCollapsible trigger={t("tab-4-question-1")}>
              {t("tab-4-answer-1")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-4-question-2")}>
              {t("tab-4-answer-2")}
            </CustomCollapsible>
            <CustomCollapsible trigger={t("tab-4-question-3")}>
              {t("tab-4-answer-3")}
            </CustomCollapsible>
          </TabPanel>
          {/* ============== END TAB 4 ================= */}
          <CustomCollapsible trigger={t("tab-last-question-1")}>
            {t("tab-last-answer-1-1")}
            <a
              className={styles["link-space"]}
              href={routesListWdia.JOIN_US}
              target="_blank"
              rel="noreferrer noopener"
            >
              {routesListWdia.JOIN_US}
            </a>
            {t("tab-last-answer-1-2")}
            <a
              className={styles["link-space"]}
              href="/"
              target="_blank"
              rel="noreferrer noopener"
            >
              https://multipass.org/
            </a>
          </CustomCollapsible>
        </Tabs>
      </div>
    </section>
  );
});
