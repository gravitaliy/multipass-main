import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import { PromoFaq } from "./components/PromoFaq/PromoFaq";
import { Form } from "./components/Form/Form";
import styles from "./Faq.module.scss";
import { ChannelsSocial } from "./components/ChannelsSocial/ChannelsSocial";
import { TabsCustom } from "./components/TabsCustom/TabsCustom";

export const Faq = memo(() => {
  const { t } = useTranslation(["faq", "common", "form"]);

  return (
    <>
      <PromoFaq t={t} />
      <TabsCustom t={t} />
      <ChannelsSocial t={t} />
      <div className={styles["faq-description-block"]}>
        <div className="c-container">
          <Form t={t} />
        </div>
      </div>
    </>
  );
});
