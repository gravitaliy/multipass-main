import React, { memo } from "react";
import clsx from "clsx";
import Link from "next/link";
import styles from "./MenuMd.module.scss";

export const MenuMd = memo(({ navbar, isOpen, openToggle }) => {
  return (
    <div className={clsx(styles["menu-md"], isOpen && styles.open, "page-new")}>
      <span onClick={openToggle} role="presentation">
        <img src="/static/media/HeaderNew/MenuMd/close.svg" alt="" />
      </span>

      {navbar.map(item => {
        if (Array.isArray(item.link)) {
          return item.link.map(p => (
            <Link href={p.link} key={p.id}>
              <a href={p.link}>{p.label}</a>
            </Link>
          ));
        }

        return (
          <Link href={item.link} key={item.id}>
            <a href={item.link}>{item.label}</a>
          </Link>
        );
      })}
    </div>
  );
});
