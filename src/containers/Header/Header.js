import React, { memo, useState } from "react";
import Link from "next/link";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import { routesList } from "../../assets/enums/routesList";
import { Logo } from "../../components/Logo/Logo";
import { ButtonMain } from "../../components/ButtonMain/ButtonMain";
import { MenuMd } from "./components/MenuMd/MenuMd";
import styles from "./Header.module.scss";
import { navData } from "../../assets/enums/navData";

export const Header = memo(() => {
  const [isOpen, setOpenToggle] = useState(false);

  const { t } = useTranslation("header");

  const openToggle = () => {
    setOpenToggle(!isOpen);
  };

  return (
    <>
      <MenuMd navbar={navData()} isOpen={isOpen} openToggle={openToggle} />
      <header className={clsx(styles.header, "page-new")}>
        <div className="c-container">
          <div className={clsx(styles["c-navbar"], "flex", "align-center")}>
            <div className={styles["c-navbar-logo"]}>
              <Logo />
            </div>
            <nav
              className={clsx(styles["c-navbar-items"], "flex", "align-center")}
            >
              {navData().map(item => {
                return (
                  <Link href={item.link} key={item.id}>
                    <a className={styles["c-navbar-item"]} href={item.link}>
                      {item.label}
                    </a>
                  </Link>
                );
              })}
            </nav>
            <div className={styles["c-navbar-right"]}>
              <ButtonMain
                href={routesList.CREATE_PASSPORT}
                target="_blank"
                rel="noreferrer noopener"
                label={t("btn")}
                color="green"
                size="xs-small"
              />
            </div>
            <button
              className={clsx(styles["navbar-toggler"], "default-style")}
              type="button"
              onClick={openToggle}
            >
              <img src="/static/media/HeaderNew/toggler-button.svg" alt="" />
            </button>
          </div>
        </div>
      </header>
    </>
  );
});
