import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import Link from "next/link";
import { Logo } from "../../components/Logo/Logo";
import { Lang } from "./components/Lang/Lang";
import { appData } from "../../assets/enums/appList";
import styles from "./Footer.module.scss";
import { SocialBlocks } from "../../components/SocialBlocks/SocialBlocks";
import { navData } from "../../assets/enums/navData";

export const Footer = memo(() => {
  const { t } = useTranslation("footer");

  const privacyData = [
    {
      link: "/static/media/Footer/Multipass_Website_Terms_Use.pdf",
      label: t("privacy-1"),
    },
    {
      link: "/static/media/Footer/Multipass_Privacy_Policy.pdf",
      label: t("privacy-2"),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <footer className={clsx(styles.footer, "page-new")}>
      <div className="c-container">
        <div className={styles["footer-top"]}>
          <div className={styles["footer-top-item"]}>
            <div className={styles["c-navbar-logo"]}>
              <Logo />
            </div>
            <div className={styles["copyright-footer"]}>
              <div>{t("copyright-1")}</div>
              <div>{t("copyright-2")}</div>
            </div>
            <div className={styles["footer-top-apps"]}>
              {appData.map(item => (
                <a
                  key={item.id}
                  className={styles["footer-top-apps-item"]}
                  href={item.href}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img className="fix-img" src={item.img} alt="" />
                </a>
              ))}
            </div>
          </div>
          <div
            className={clsx(styles["footer-top-item"], styles["footer-navbar"])}
          >
            <nav className={styles["footer-navbar__items"]}>
              {navData().map(item => {
                return (
                  <Link href={item.link} key={item.id}>
                    <a
                      className={styles["c-footer-navbar-item"]}
                      href={item.link}
                    >
                      {item.label}
                    </a>
                  </Link>
                );
              })}
            </nav>
            <Lang />
          </div>
        </div>
      </div>
      <div className={styles["footer-bottom-content"]}>
        <div className="c-container">
          <div className={styles["footer-bottom-content-inner"]}>
            <ul className={clsx(styles["footer-links"], "default-style")}>
              {privacyData.map(item => (
                <li className={styles["footer-links-item"]} key={item.id}>
                  <a
                    className={styles["footer-links-item-link"]}
                    href={item.link}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {item.label}
                  </a>
                </li>
              ))}
            </ul>
            <div className={styles["footer-bottom-socials-wrap"]}>
              <SocialBlocks />
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
});
